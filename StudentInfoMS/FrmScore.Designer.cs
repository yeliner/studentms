﻿namespace StudentInfoMS
{
    partial class FrmScore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtscoreid = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtscore = new System.Windows.Forms.TextBox();
            this.txtremark = new System.Windows.Forms.TextBox();
            this.cbostudent = new System.Windows.Forms.ComboBox();
            this.cbosubject = new System.Windows.Forms.ComboBox();
            this.btnselect = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            this.btnre = new System.Windows.Forms.Button();
            this.cbstudnet = new System.Windows.Forms.CheckBox();
            this.cbclass = new System.Windows.Forms.CheckBox();
            this.dgvscore = new System.Windows.Forms.DataGridView();
            this.dtptime = new System.Windows.Forms.DateTimePicker();
            this.cbtime = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvscore)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "成绩编号";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(294, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "考试时间";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(70, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "学    生";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(294, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "科    目";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(70, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "分    数";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(277, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "备    注";
            // 
            // txtscoreid
            // 
            this.txtscoreid.Enabled = false;
            this.txtscoreid.Location = new System.Drawing.Point(129, 31);
            this.txtscoreid.Name = "txtscoreid";
            this.txtscoreid.Size = new System.Drawing.Size(142, 21);
            this.txtscoreid.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbtime);
            this.panel1.Controls.Add(this.dtptime);
            this.panel1.Controls.Add(this.cbclass);
            this.panel1.Controls.Add(this.cbstudnet);
            this.panel1.Controls.Add(this.btnre);
            this.panel1.Controls.Add(this.btndelete);
            this.panel1.Controls.Add(this.btnsave);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnselect);
            this.panel1.Controls.Add(this.cbosubject);
            this.panel1.Controls.Add(this.cbostudent);
            this.panel1.Controls.Add(this.txtremark);
            this.panel1.Controls.Add(this.txtscore);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(17, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(624, 183);
            this.panel1.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(3, 189);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(624, 183);
            this.panel2.TabIndex = 8;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvscore);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Location = new System.Drawing.Point(17, 209);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(624, 183);
            this.panel3.TabIndex = 8;
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(3, 189);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(624, 183);
            this.panel4.TabIndex = 8;
            // 
            // txtscore
            // 
            this.txtscore.Location = new System.Drawing.Point(112, 80);
            this.txtscore.Name = "txtscore";
            this.txtscore.Size = new System.Drawing.Size(142, 21);
            this.txtscore.TabIndex = 10;
            // 
            // txtremark
            // 
            this.txtremark.Location = new System.Drawing.Point(349, 80);
            this.txtremark.Name = "txtremark";
            this.txtremark.Size = new System.Drawing.Size(142, 21);
            this.txtremark.TabIndex = 11;
            // 
            // cbostudent
            // 
            this.cbostudent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbostudent.FormattingEnabled = true;
            this.cbostudent.Location = new System.Drawing.Point(112, 50);
            this.cbostudent.Name = "cbostudent";
            this.cbostudent.Size = new System.Drawing.Size(112, 20);
            this.cbostudent.TabIndex = 12;
            // 
            // cbosubject
            // 
            this.cbosubject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosubject.FormattingEnabled = true;
            this.cbosubject.Location = new System.Drawing.Point(349, 47);
            this.cbosubject.Name = "cbosubject";
            this.cbosubject.Size = new System.Drawing.Size(117, 20);
            this.cbosubject.TabIndex = 13;
            // 
            // btnselect
            // 
            this.btnselect.Location = new System.Drawing.Point(55, 128);
            this.btnselect.Name = "btnselect";
            this.btnselect.Size = new System.Drawing.Size(75, 23);
            this.btnselect.TabIndex = 14;
            this.btnselect.Text = "查询";
            this.btnselect.UseVisualStyleBackColor = true;
            this.btnselect.Click += new System.EventHandler(this.btnselect_Click);
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(164, 128);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 23);
            this.btnsave.TabIndex = 15;
            this.btnsave.Text = "保存";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btndelete
            // 
            this.btndelete.Location = new System.Drawing.Point(292, 128);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(75, 23);
            this.btndelete.TabIndex = 16;
            this.btndelete.Text = "删除";
            this.btndelete.UseVisualStyleBackColor = true;
            this.btndelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // btnre
            // 
            this.btnre.Location = new System.Drawing.Point(416, 128);
            this.btnre.Name = "btnre";
            this.btnre.Size = new System.Drawing.Size(75, 23);
            this.btnre.TabIndex = 17;
            this.btnre.Text = "新填";
            this.btnre.UseVisualStyleBackColor = true;
            this.btnre.Click += new System.EventHandler(this.btnre_Click);
            // 
            // cbstudnet
            // 
            this.cbstudnet.AutoSize = true;
            this.cbstudnet.Location = new System.Drawing.Point(230, 52);
            this.cbstudnet.Name = "cbstudnet";
            this.cbstudnet.Size = new System.Drawing.Size(15, 14);
            this.cbstudnet.TabIndex = 18;
            this.cbstudnet.UseVisualStyleBackColor = true;
            // 
            // cbclass
            // 
            this.cbclass.AutoSize = true;
            this.cbclass.Location = new System.Drawing.Point(472, 49);
            this.cbclass.Name = "cbclass";
            this.cbclass.Size = new System.Drawing.Size(15, 14);
            this.cbclass.TabIndex = 19;
            this.cbclass.UseVisualStyleBackColor = true;
            // 
            // dgvscore
            // 
            this.dgvscore.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvscore.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvscore.Location = new System.Drawing.Point(0, 0);
            this.dgvscore.Name = "dgvscore";
            this.dgvscore.RowTemplate.Height = 23;
            this.dgvscore.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvscore.Size = new System.Drawing.Size(621, 180);
            this.dgvscore.TabIndex = 9;
            this.dgvscore.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvscore_CellClick);
            // 
            // dtptime
            // 
            this.dtptime.Location = new System.Drawing.Point(349, 11);
            this.dtptime.Name = "dtptime";
            this.dtptime.Size = new System.Drawing.Size(117, 21);
            this.dtptime.TabIndex = 20;
            // 
            // cbtime
            // 
            this.cbtime.AutoSize = true;
            this.cbtime.Location = new System.Drawing.Point(472, 17);
            this.cbtime.Name = "cbtime";
            this.cbtime.Size = new System.Drawing.Size(15, 14);
            this.cbtime.TabIndex = 21;
            this.cbtime.UseVisualStyleBackColor = true;
            // 
            // FrmScore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 404);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.txtscoreid);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "FrmScore";
            this.Text = "成绩";
            this.Load += new System.EventHandler(this.FrmScore_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvscore)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtscoreid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox cbclass;
        private System.Windows.Forms.CheckBox cbstudnet;
        private System.Windows.Forms.Button btnre;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnselect;
        private System.Windows.Forms.ComboBox cbosubject;
        private System.Windows.Forms.ComboBox cbostudent;
        private System.Windows.Forms.TextBox txtremark;
        private System.Windows.Forms.TextBox txtscore;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dgvscore;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DateTimePicker dtptime;
        private System.Windows.Forms.CheckBox cbtime;
    }
}