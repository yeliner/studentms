﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace StudentInfoMS
{
    /// <summary>
    /// 编辑学生界面
    /// </summary>
    public partial class FrmEditStudent : Form
    {
        public FrmEditStudent()
        {
            InitializeComponent();
        }
        public int studentid = 0;//判断id
        private void FrmEditStudent_Load(object sender, EventArgs e)
        {
            showstudents();
            classname();
        }

        private void showstudents()
        {//显示学生信息
            if (studentid != 0)
            {
                string sql = "select * from dbo.tb_students where id=" + studentid;
                try
                {
                    DatabaseHelper.connection.Open();
                    SqlCommand com = new SqlCommand(sql, DatabaseHelper.connection);
                    SqlDataReader r = com.ExecuteReader();
                    while (r.Read())
                    {
                        txtStudentId.Text = r["id"].ToString();
                        this.txtName.Text = r["studentname"].ToString(); txtIdCard.Text = r["idcard"].ToString();
                        if (r["gender"].ToString() == "男")
                        {
                            rbMale.Checked = true;
                        }
                        else
                        {
                            rbFemale.Checked = true;
                        }

                        dtpBirth.Text = r["birthdate"].ToString(); txtNation.Text = r["nation"].ToString(); txtJob.Text = r["job"].ToString();
                        txtCity.Text = r["city"].ToString(); cboClass.Text = r["class"].ToString(); txtRemark.Text = r["remark"].ToString();
                    }
                    r.Close();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    DatabaseHelper.connection.Close();
                }
            }

        }

        private void classname()
        {
            //显示班级
            string sql = "select  className from dbo.tb_classes";
            try
            {
                DatabaseHelper.connection.Open();
                SqlCommand com = new SqlCommand(sql, DatabaseHelper.connection);
                SqlDataReader r = com.ExecuteReader();

                while (r.Read())
                {
                    cboClass.Items.Add(r["classname"]);
                }
                r.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                DatabaseHelper.connection.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {//保存功能
            if (this.txtName.Text.Trim() =="")
            {
                MessageBox.Show("名字不能为空!");
                return;
            }
            if (this.txtIdCard.Text.Trim() =="")
            {
                MessageBox.Show("身份证号不能为空!");
                return;
            }
            if (this.cboClass.Text.Trim() =="")
            {
                MessageBox.Show("班级不能为空!");
                return;
            }

            string sql ="";
            if (studentid == 0)
            {//添加
                sql = string.Format("insert into tb_students  values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}');",
                          this.txtName.Text, txtIdCard.Text, rbFemale.Checked ? "女" : "男",
                       dtpBirth.Value.ToString(), txtNation.Text, txtJob.Text, txtCity.Text, cboClass.Text, txtRemark.Text);

            }
            else
            {//修改
                sql = string.Format("update tb_students set studentName='{0}',idcard='{1}',gender ='{2}',birthdate ='{3}' ,nation ='{4}',job ='{5}',city ='{6}' ,class ='{7}' ,remark ='{8}' where id={9} ", this.txtName.Text, txtIdCard.Text, rbFemale.Checked ? "女" : "男",
                       dtpBirth.Value.ToString(), txtNation.Text, txtJob.Text, txtCity.Text, cboClass.Text, txtRemark.Text,txtStudentId.Text);
            }
            try
            {
                DatabaseHelper.connection.Open();
                SqlCommand com = new SqlCommand(sql, DatabaseHelper.connection);
                int num = com.ExecuteNonQuery();
                if (num > 0)
                {
                    MessageBox.Show("保存成功!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                DatabaseHelper.connection.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {//取消,清空
            this.txtName.Text = ""; txtIdCard.Text = ""; txtNation.Text = ""; txtJob.Text = ""; txtCity.Text = ""; cboClass.Text = ""; txtRemark.Text = "";
        }
    }


}

