﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace StudentInfoMS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
       public  DataSet ds =null;
       public  SqlDataAdapter da = null;
        private void Form1_Load(object sender, EventArgs e)
        {
            loadclass();

        }

        public  void loadclass()
        {//加载班级方法
            ds= new DataSet();
            string sql = "  select id as id, className as 班级名称,openDate as 开班时间,grade as 年级,remark as 备注 from tb_classes";
          da = new SqlDataAdapter(sql, DatabaseHelper.connection);
            da.Fill(ds);
            dgvclass.DataSource = ds.Tables[0];
            
        }

        private void btnresave_Click(object sender, EventArgs e)
        {
            //更新
            SqlCommandBuilder buid = new SqlCommandBuilder(da);
            da.Update(ds);
            loadclass();
        }
    }
}
