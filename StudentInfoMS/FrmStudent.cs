﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace StudentInfoMS
{
    /// <summary>
    /// 学生查询界面
    /// </summary>
    public partial class FrmStudent : Form
    {
        public FrmStudent()
        {
            InitializeComponent();
        }

        private void FrmStudent_Load(object sender, EventArgs e)
        {
            TreenodesStudents();
        }

        private void TreenodesStudents()//添加节点
        {//清空节点
            tvStudent.Nodes.Clear();
            //创父节点
            TreeNode tr = new TreeNode("所有学生");
            tvStudent.Nodes.Add(tr);
            string sql = "select className from dbo.tb_classes";
            try
            {
                DatabaseHelper.connection.Open();
                SqlCommand com = new SqlCommand(sql, DatabaseHelper.connection);
                SqlDataReader r = com.ExecuteReader();
                while (r.Read())
                {
                    string classname=r["classname"].ToString();
                    tr.Nodes.Add(classname);
                }
                r.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                DatabaseHelper.connection.Close();
            }
            tvStudent.ExpandAll();


        }

        private void tvStudent_AfterSelect(object sender, TreeViewEventArgs e)
        {
            loadstudents();
        }

        private void loadstudents()
        {
            string sql = "select * from tb_students";
            if (tvStudent.SelectedNode!=null&&tvStudent.SelectedNode.Text!="所有学生")
            {
                sql =string.Format("select * from tb_students where class='{0}'", tvStudent.SelectedNode.Text);
            }
            try
            {
                DatabaseHelper.connection.Open();
                SqlCommand com = new SqlCommand(sql, DatabaseHelper.connection);
                SqlDataReader r = com.ExecuteReader();
                lvStudents.Items.Clear();
                while (r.Read())
                {
                    string id = r["id"].ToString();
                    ListViewItem item = new ListViewItem(id);
                    item.SubItems.AddRange(new string[] {
                        r["studentname"].ToString(), r["idcard"].ToString(), r["gender"].ToString(), r["birthdate"].ToString(), r["nation"].ToString(), r["job"].ToString(),r["city"].ToString(),
                    r["class"].ToString(),r["remark"].ToString(),});
                    lvStudents.Items.Add(item);

                }
                r.Close();                                                                                                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            DatabaseHelper.connection.Close();
        }

        private void tsmiEdit_Click(object sender, EventArgs e)
        {
            //编辑学生信息
            if (lvStudents.SelectedItems.Count==0)
            {
                MessageBox.Show("请选择要修改的学生");
                return;
            }
            int id =int.Parse( lvStudents.SelectedItems[0].Text);

            FrmEditStudent frmEditStudent = new FrmEditStudent();

            frmEditStudent.studentid = id;
            frmEditStudent.Show(); 
        }

        private void tsmiDelete_Click(object sender, EventArgs e)
        {
            //删除学生信息
            if (lvStudents.SelectedItems.Count == 0)
            {
                MessageBox.Show("请选择要删除的学生");
                return;
            }
            int id = int.Parse(lvStudents.SelectedItems[0].Text);
            if (MessageBox.Show ("是否删除选择的该学生信息?","提示",MessageBoxButtons.OKCancel)==DialogResult.OK)
            {
                string sql = string.Format("delete dbo.tb_students where id=''" + id);
                try
                {
                    DatabaseHelper.connection.Open();
                    SqlCommand com = new SqlCommand(sql, DatabaseHelper.connection);
                    int num = com.ExecuteNonQuery();
                    if (num > 0)
                    {
                        MessageBox.Show("删除成功!");
                    }
                    this.lvStudents.SelectedItems[0].Remove();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    DatabaseHelper.connection.Close();
                }
            }
            

        }

        private void tsmiRefresh_Click(object sender, EventArgs e)
        {
            loadstudents();
        }

      

     
    }
}
