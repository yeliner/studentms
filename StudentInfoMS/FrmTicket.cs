﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StudentInfoMS
{
    public partial class FrmTicket : Form
    {
        public FrmTicket()
        {
            InitializeComponent();
        }

        private void dgvTicketinfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void FrmTicket_Load(object sender, EventArgs e)
        {
            loadtiketinfo();
        }

        private void loadtiketinfo()
        {
            string sql;
            if (txtfrom.Text.Trim()==""&&txtto.Text.Trim()=="")
            {
              sql = "select Id,FlightNO,LeaveCity,Destination,LeaveDate  from TicketInfo  ";
            }
            DataSet ds = DbHelper.executeDataSet(sql);
            dgvTicketinfo.DataSource = ds.Tables[0];
        }

        private void dgvTicketinfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvTicketinfo.SelectedRows.Count>0)
            {
                string sql = "select LeaveCity,Destination,LeaveDate,FirstClass,SecondClass  from TicketInfo where id=" + dgvTicketinfo.SelectedRows[0].Cells[0].Value.ToString();
                 DataSet ds = DbHelper.executeDataSet(sql);
                 this.txtleave.Text = ds.Tables[0].Rows[0]["LeaveCity"].ToString();
                 this.txtfinal.Text = ds.Tables[0].Rows[0]["Destination"].ToString();
                 this.txtleavetime.Text = ds.Tables[0].Rows[0]["LeaveDate"].ToString();
                 this.txtf.Text = ds.Tables[0].Rows[0]["FirstClass"].ToString();
                 this.txts.Text = ds.Tables[0].Rows[0]["SecondClass"].ToString();
            }
        }

        private void btnselect_Click(object sender, EventArgs e)
        {
            loadtiketinfo();
        }

    }
}
