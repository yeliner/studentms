﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace StudentInfoMS
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (ValiInput())
            {
                if (ValiLogin())
                {
                    FrmMain main = new FrmMain();
                    this.Visible = false;
                    main.Show();
                }
                else
                {
                    MessageBox.Show("账号或者密码错误");
                }
            }
        }
        //非空判断
        private bool ValiInput()
        {
            if (txtUsername.Text.Trim() == "")
            {
                MessageBox.Show("用户名不能为空");
                txtUsername.Focus();
                return false;
            }
            if (txtPassword.Text.Trim() == "")
            {
                MessageBox.Show("密码不能为空");
                txtUsername.Focus();
                return false;
            }
            return true;
        }
        //判断数据库里是否有数据
        private bool ValiLogin()
        {
            //字符串连接
            string connstring = "data source=.;initial catalog=studentInfoDb;User ID=sa;Pwd=123.com";
            //创建Connection对象
            SqlConnection connection = new SqlConnection(connstring);
            int num = 0;
            //SQL语句
            string sql = string.Format("select count(*) from dbo.tb_users where username='{0}'and password='{1}'",
            txtUsername.Text, txtPassword.Text);
            try
            {
                //数据库打开
                connection.Open();
                //创建Command对象
                SqlCommand command = new SqlCommand(sql, connection);
                num = (int)command.ExecuteScalar();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return num > 0;
        }
    }
}
