﻿namespace StudentInfoMS
{
    partial class FrmTicket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtfrom = new System.Windows.Forms.TextBox();
            this.txtto = new System.Windows.Forms.TextBox();
            this.btnselect = new System.Windows.Forms.Button();
            this.dgvTicketinfo = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.leave = new System.Windows.Forms.Label();
            this.txtleave = new System.Windows.Forms.TextBox();
            this.txtfinal = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtleavetime = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtf = new System.Windows.Forms.TextBox();
            this.txts = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNo = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtnum = new System.Windows.Forms.TextBox();
            this.cbotype = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtdate = new System.Windows.Forms.TextBox();
            this.btnbook = new System.Windows.Forms.Button();
            this.btnclose = new System.Windows.Forms.Button();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTicketinfo)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "第一步,航班信息查询";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "出发地:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(236, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "目的地:";
            // 
            // txtfrom
            // 
            this.txtfrom.Location = new System.Drawing.Point(94, 39);
            this.txtfrom.Name = "txtfrom";
            this.txtfrom.Size = new System.Drawing.Size(100, 21);
            this.txtfrom.TabIndex = 2;
            // 
            // txtto
            // 
            this.txtto.Location = new System.Drawing.Point(289, 39);
            this.txtto.Name = "txtto";
            this.txtto.Size = new System.Drawing.Size(100, 21);
            this.txtto.TabIndex = 3;
            // 
            // btnselect
            // 
            this.btnselect.Location = new System.Drawing.Point(414, 39);
            this.btnselect.Name = "btnselect";
            this.btnselect.Size = new System.Drawing.Size(75, 23);
            this.btnselect.TabIndex = 4;
            this.btnselect.Text = "查询";
            this.btnselect.UseVisualStyleBackColor = true;
            this.btnselect.Click += new System.EventHandler(this.btnselect_Click);
            // 
            // dgvTicketinfo
            // 
            this.dgvTicketinfo.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.dgvTicketinfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTicketinfo.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvTicketinfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTicketinfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dgvTicketinfo.Location = new System.Drawing.Point(43, 66);
            this.dgvTicketinfo.Name = "dgvTicketinfo";
            this.dgvTicketinfo.RowTemplate.Height = 23;
            this.dgvTicketinfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTicketinfo.Size = new System.Drawing.Size(562, 183);
            this.dgvTicketinfo.TabIndex = 5;
            this.dgvTicketinfo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTicketinfo_CellClick);
            this.dgvTicketinfo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTicketinfo_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txts);
            this.groupBox1.Controls.Add(this.txtf);
            this.groupBox1.Controls.Add(this.txtfinal);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtleavetime);
            this.groupBox1.Controls.Add(this.txtleave);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.leave);
            this.groupBox1.Location = new System.Drawing.Point(43, 255);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 123);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "航班详细信息";
            // 
            // leave
            // 
            this.leave.AutoSize = true;
            this.leave.Location = new System.Drawing.Point(41, 20);
            this.leave.Name = "leave";
            this.leave.Size = new System.Drawing.Size(47, 12);
            this.leave.TabIndex = 2;
            this.leave.Text = "出发地:";
            // 
            // txtleave
            // 
            this.txtleave.Location = new System.Drawing.Point(110, 20);
            this.txtleave.Name = "txtleave";
            this.txtleave.ReadOnly = true;
            this.txtleave.Size = new System.Drawing.Size(100, 21);
            this.txtleave.TabIndex = 3;
            // 
            // txtfinal
            // 
            this.txtfinal.Location = new System.Drawing.Point(110, 45);
            this.txtfinal.Name = "txtfinal";
            this.txtfinal.ReadOnly = true;
            this.txtfinal.Size = new System.Drawing.Size(100, 21);
            this.txtfinal.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "目的地:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(41, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 4;
            this.label6.Text = "出发时间:";
            // 
            // txtleavetime
            // 
            this.txtleavetime.Location = new System.Drawing.Point(110, 73);
            this.txtleavetime.Name = "txtleavetime";
            this.txtleavetime.ReadOnly = true;
            this.txtleavetime.Size = new System.Drawing.Size(100, 21);
            this.txtleavetime.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(244, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 12);
            this.label7.TabIndex = 2;
            this.label7.Text = "经济舱票价:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(244, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 12);
            this.label8.TabIndex = 2;
            this.label8.Text = "头等舱票价:";
            // 
            // txtf
            // 
            this.txtf.Location = new System.Drawing.Point(321, 17);
            this.txtf.Name = "txtf";
            this.txtf.ReadOnly = true;
            this.txtf.Size = new System.Drawing.Size(100, 21);
            this.txtf.TabIndex = 6;
            // 
            // txts
            // 
            this.txts.Location = new System.Drawing.Point(321, 45);
            this.txts.Name = "txts";
            this.txts.ReadOnly = true;
            this.txts.Size = new System.Drawing.Size(100, 21);
            this.txts.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtdate);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.cbotype);
            this.groupBox2.Controls.Add(this.txtnum);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtNo);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Location = new System.Drawing.Point(43, 400);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(562, 82);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "机票预订";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(41, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 4;
            this.label10.Text = "预订数量";
            // 
            // txtNo
            // 
            this.txtNo.Location = new System.Drawing.Point(110, 20);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(100, 21);
            this.txtNo.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(244, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 2;
            this.label12.Text = "舱位类型";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(41, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 2;
            this.label13.Text = "航班号";
            // 
            // txtnum
            // 
            this.txtnum.Location = new System.Drawing.Point(110, 48);
            this.txtnum.Name = "txtnum";
            this.txtnum.Size = new System.Drawing.Size(100, 21);
            this.txtnum.TabIndex = 5;
            // 
            // cbotype
            // 
            this.cbotype.FormattingEnabled = true;
            this.cbotype.Location = new System.Drawing.Point(303, 17);
            this.cbotype.Name = "cbotype";
            this.cbotype.Size = new System.Drawing.Size(121, 20);
            this.cbotype.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(244, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 7;
            this.label9.Text = "出发日期";
            // 
            // txtdate
            // 
            this.txtdate.Location = new System.Drawing.Point(303, 48);
            this.txtdate.Name = "txtdate";
            this.txtdate.Size = new System.Drawing.Size(121, 21);
            this.txtdate.TabIndex = 8;
            // 
            // btnbook
            // 
            this.btnbook.Location = new System.Drawing.Point(438, 500);
            this.btnbook.Name = "btnbook";
            this.btnbook.Size = new System.Drawing.Size(75, 23);
            this.btnbook.TabIndex = 9;
            this.btnbook.Text = "预订";
            this.btnbook.UseVisualStyleBackColor = true;
            // 
            // btnclose
            // 
            this.btnclose.Location = new System.Drawing.Point(530, 500);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(75, 23);
            this.btnclose.TabIndex = 10;
            this.btnclose.Text = "关闭";
            this.btnclose.UseVisualStyleBackColor = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "FlightNO";
            this.Column1.HeaderText = "航班号";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "LeaveCity";
            this.Column2.HeaderText = "出发城市";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Destination";
            this.Column3.HeaderText = "目的城市";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "LeaveDate";
            this.Column4.HeaderText = "出发时间";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Id";
            this.Column5.HeaderText = "id";
            this.Column5.Name = "Column5";
            this.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column5.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(429, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "label4";
            // 
            // FrmTicket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 535);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.btnbook);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvTicketinfo);
            this.Controls.Add(this.btnselect);
            this.Controls.Add(this.txtto);
            this.Controls.Add(this.txtfrom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmTicket";
            this.Text = "航班查询及预订";
            this.Load += new System.EventHandler(this.FrmTicket_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTicketinfo)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtfrom;
        private System.Windows.Forms.TextBox txtto;
        private System.Windows.Forms.Button btnselect;
        private System.Windows.Forms.DataGridView dgvTicketinfo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txts;
        private System.Windows.Forms.TextBox txtf;
        private System.Windows.Forms.TextBox txtfinal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtleavetime;
        private System.Windows.Forms.TextBox txtleave;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label leave;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtdate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbotype;
        private System.Windows.Forms.TextBox txtnum;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtNo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnbook;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.Label label4;
    }
}