﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentInfoMS
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void tsmiClass_Click(object sender, EventArgs e)
        {
         
        }

        private void FrmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void tsmiCourse_Click(object sender, EventArgs e)
        {
            
        }

        private void tsmiStudent_Click(object sender, EventArgs e)
        {

        }

        private void tsmiScore_Click(object sender, EventArgs e)
        {
            
        }

        private void tsmiAddStudent_Click(object sender, EventArgs e)
        {
            FrmEditStudent frmEditStudent = new FrmEditStudent();
            frmEditStudent.MdiParent = this;
            frmEditStudent.Show();
        }

        private void tsmiQueryStudent_Click(object sender, EventArgs e)
        {
            FrmStudent frmStu = new FrmStudent();
            frmStu.MdiParent = this;
            frmStu.Show();
        }

        private void tsmiPassword_Click(object sender, EventArgs e)
        {

        }
    }
}
