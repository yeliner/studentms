﻿namespace StudentInfoMS
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmiBasic = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiClass = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiStudent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiQueryStudent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAddStudent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCourse = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiScore = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSystem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmisUser = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPassword = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiBasic,
            this.tsmiSystem,
            this.tsmiHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(3, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(736, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsmiBasic
            // 
            this.tsmiBasic.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiClass,
            this.tsmiStudent,
            this.tsmiCourse,
            this.tsmiScore});
            this.tsmiBasic.Name = "tsmiBasic";
            this.tsmiBasic.Size = new System.Drawing.Size(84, 22);
            this.tsmiBasic.Text = "基本信息(&B)";
            // 
            // tsmiClass
            // 
            this.tsmiClass.Name = "tsmiClass";
            this.tsmiClass.Size = new System.Drawing.Size(142, 22);
            this.tsmiClass.Text = "班级信息(&C)";
            this.tsmiClass.Click += new System.EventHandler(this.tsmiClass_Click);
            // 
            // tsmiStudent
            // 
            this.tsmiStudent.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiQueryStudent,
            this.tsmiAddStudent});
            this.tsmiStudent.Name = "tsmiStudent";
            this.tsmiStudent.Size = new System.Drawing.Size(142, 22);
            this.tsmiStudent.Text = "学生信息(&S)";
            this.tsmiStudent.Click += new System.EventHandler(this.tsmiStudent_Click);
            // 
            // tsmiQueryStudent
            // 
            this.tsmiQueryStudent.Name = "tsmiQueryStudent";
            this.tsmiQueryStudent.Size = new System.Drawing.Size(166, 22);
            this.tsmiQueryStudent.Text = "查询学生信息(&Q)";
            this.tsmiQueryStudent.Click += new System.EventHandler(this.tsmiQueryStudent_Click);
            // 
            // tsmiAddStudent
            // 
            this.tsmiAddStudent.Name = "tsmiAddStudent";
            this.tsmiAddStudent.Size = new System.Drawing.Size(166, 22);
            this.tsmiAddStudent.Text = "添加学生信息(&A)";
            this.tsmiAddStudent.Click += new System.EventHandler(this.tsmiAddStudent_Click);
            // 
            // tsmiCourse
            // 
            this.tsmiCourse.Name = "tsmiCourse";
            this.tsmiCourse.Size = new System.Drawing.Size(142, 22);
            this.tsmiCourse.Text = "课程信息(&O)";
            this.tsmiCourse.Click += new System.EventHandler(this.tsmiCourse_Click);
            // 
            // tsmiScore
            // 
            this.tsmiScore.Name = "tsmiScore";
            this.tsmiScore.Size = new System.Drawing.Size(142, 22);
            this.tsmiScore.Text = "成绩管理(&P)";
            this.tsmiScore.Click += new System.EventHandler(this.tsmiScore_Click);
            // 
            // tsmiSystem
            // 
            this.tsmiSystem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmisUser,
            this.tsmiPassword});
            this.tsmiSystem.Name = "tsmiSystem";
            this.tsmiSystem.Size = new System.Drawing.Size(83, 22);
            this.tsmiSystem.Text = "系统管理(&S)";
            // 
            // tsmisUser
            // 
            this.tsmisUser.Name = "tsmisUser";
            this.tsmisUser.Size = new System.Drawing.Size(152, 22);
            this.tsmisUser.Text = "用户管理(&U)";
            // 
            // tsmiPassword
            // 
            this.tsmiPassword.Name = "tsmiPassword";
            this.tsmiPassword.Size = new System.Drawing.Size(152, 22);
            this.tsmiPassword.Text = "修改密码(&C)";
            this.tsmiPassword.Click += new System.EventHandler(this.tsmiPassword_Click);
            // 
            // tsmiHelp
            // 
            this.tsmiHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAbout});
            this.tsmiHelp.Name = "tsmiHelp";
            this.tsmiHelp.Size = new System.Drawing.Size(61, 22);
            this.tsmiHelp.Text = "帮助(&H)";
            // 
            // tsmiAbout
            // 
            this.tsmiAbout.Name = "tsmiAbout";
            this.tsmiAbout.Size = new System.Drawing.Size(116, 22);
            this.tsmiAbout.Text = "关于(&A)";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 478);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrmMain";
            this.Text = "学生信息管理系统";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMain_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiBasic;
        private System.Windows.Forms.ToolStripMenuItem tsmiClass;
        private System.Windows.Forms.ToolStripMenuItem tsmiStudent;
        private System.Windows.Forms.ToolStripMenuItem tsmiCourse;
        private System.Windows.Forms.ToolStripMenuItem tsmiScore;
        private System.Windows.Forms.ToolStripMenuItem tsmiSystem;
        private System.Windows.Forms.ToolStripMenuItem tsmisUser;
        private System.Windows.Forms.ToolStripMenuItem tsmiPassword;
        private System.Windows.Forms.ToolStripMenuItem tsmiHelp;
        private System.Windows.Forms.ToolStripMenuItem tsmiAbout;
        private System.Windows.Forms.ToolStripMenuItem tsmiQueryStudent;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddStudent;
    }
}