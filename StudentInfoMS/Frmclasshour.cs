﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace StudentInfoMS
{
    public partial class Frmclasshour : Form
    {
        public Frmclasshour()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Frmclasshour_Load(object sender, EventArgs e)
        {
            loadclass();
        }
        public void loadclass()
        {//加载班级方法
            DataSet ds = new DataSet();
            string sql = " select ID as id, coursename as 课程名称,grade as 年级 from tb_courses";
            SqlDataAdapter da = new SqlDataAdapter(sql, DatabaseHelper.connection);
            da.Fill(ds);
            dvgcourse.DataSource = ds.Tables[0];

        }

        private void dvgcourse_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.txtid.Text = dvgcourse.SelectedRows[0].Cells[0].Value.ToString();
            this.txtclassname.Text = dvgcourse.SelectedRows[0].Cells[1].Value.ToString();
          this.grade.Text=  dvgcourse.SelectedRows[0].Cells[2].Value.ToString();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
             string sql="";
           if (txtid.Text.Trim()!="")
           {//更新
               sql = string.Format("update tb_courses set courseName='{0}',grade='{1}' where id='{2}'", txtclassname.Text, grade.Text, dvgcourse.SelectedRows[0].Cells[0].Value.ToString());
           }
           else
           {
              sql=string.Format("insert into tb_courses values ('{0}','{1}');",txtclassname.Text,grade.Text);
           }
            
           try
           {
               DatabaseHelper.connection.Open();
               SqlCommand com = new SqlCommand(sql, DatabaseHelper.connection);
               int num = com.ExecuteNonQuery();
               if (num > 0)
               {
                   MessageBox.Show("保存成功!");
               }
           }
           catch (Exception ex)
           {

               MessageBox.Show(ex.Message);
           }
           finally
           {
               DatabaseHelper.connection.Close();
               loadclass();
           }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {//删除
            string sql = string.Format(" delete dbo.tb_courses where id='{0}'", dvgcourse.SelectedRows[0].Cells[0].Value.ToString());
            try
            {
                DatabaseHelper.connection.Open();
                SqlCommand com = new SqlCommand(sql, DatabaseHelper.connection);
                int num = com.ExecuteNonQuery();
                if (num > 0)
                {
                    MessageBox.Show("删除成功!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                DatabaseHelper.connection.Close();
            }
            loadclass();
        }

        private void btnre_Click(object sender, EventArgs e)
        {
            //重填
            txtclassname.Text = "";
            grade.Text = "";
        }
    }
}
