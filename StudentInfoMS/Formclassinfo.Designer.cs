﻿namespace StudentInfoMS
{
    partial class frmclass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvclass = new System.Windows.Forms.DataGridView();
            this.btnre = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.txtremark = new System.Windows.Forms.TextBox();
            this.cboclass = new System.Windows.Forms.ComboBox();
            this.dtpopen = new System.Windows.Forms.DateTimePicker();
            this.txtclassname = new System.Windows.Forms.TextBox();
            this.txtid = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblclassname = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblid = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvclass)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dgvclass);
            this.groupBox1.Controls.Add(this.btnre);
            this.groupBox1.Controls.Add(this.btndelete);
            this.groupBox1.Controls.Add(this.btnsave);
            this.groupBox1.Controls.Add(this.txtremark);
            this.groupBox1.Controls.Add(this.cboclass);
            this.groupBox1.Controls.Add(this.dtpopen);
            this.groupBox1.Controls.Add(this.txtclassname);
            this.groupBox1.Controls.Add(this.txtid);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lblclassname);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblid);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(573, 348);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // dgvclass
            // 
            this.dgvclass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvclass.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvclass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvclass.Location = new System.Drawing.Point(12, 182);
            this.dgvclass.Name = "dgvclass";
            this.dgvclass.RowTemplate.Height = 23;
            this.dgvclass.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvclass.Size = new System.Drawing.Size(555, 181);
            this.dgvclass.TabIndex = 13;
            this.dgvclass.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvclass_CellClick);
            // 
            // btnre
            // 
            this.btnre.Location = new System.Drawing.Point(344, 153);
            this.btnre.Name = "btnre";
            this.btnre.Size = new System.Drawing.Size(75, 23);
            this.btnre.TabIndex = 12;
            this.btnre.Text = "重填";
            this.btnre.UseVisualStyleBackColor = true;
            this.btnre.Click += new System.EventHandler(this.btnre_Click);
            // 
            // btndelete
            // 
            this.btndelete.Location = new System.Drawing.Point(211, 153);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(75, 23);
            this.btndelete.TabIndex = 11;
            this.btndelete.Text = "删除";
            this.btndelete.UseVisualStyleBackColor = true;
            this.btndelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(72, 153);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 23);
            this.btnsave.TabIndex = 10;
            this.btnsave.Text = "保存";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // txtremark
            // 
            this.txtremark.Location = new System.Drawing.Point(94, 88);
            this.txtremark.Multiline = true;
            this.txtremark.Name = "txtremark";
            this.txtremark.Size = new System.Drawing.Size(401, 43);
            this.txtremark.TabIndex = 5;
            // 
            // cboclass
            // 
            this.cboclass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboclass.FormattingEnabled = true;
            this.cboclass.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cboclass.Location = new System.Drawing.Point(366, 50);
            this.cboclass.Name = "cboclass";
            this.cboclass.Size = new System.Drawing.Size(129, 20);
            this.cboclass.TabIndex = 4;
            // 
            // dtpopen
            // 
            this.dtpopen.Location = new System.Drawing.Point(94, 53);
            this.dtpopen.Name = "dtpopen";
            this.dtpopen.Size = new System.Drawing.Size(129, 21);
            this.dtpopen.TabIndex = 3;
            // 
            // txtclassname
            // 
            this.txtclassname.Location = new System.Drawing.Point(366, 20);
            this.txtclassname.Name = "txtclassname";
            this.txtclassname.Size = new System.Drawing.Size(129, 21);
            this.txtclassname.TabIndex = 2;
            // 
            // txtid
            // 
            this.txtid.Enabled = false;
            this.txtid.Location = new System.Drawing.Point(94, 20);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(129, 21);
            this.txtid.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "开班日期";
            // 
            // lblclassname
            // 
            this.lblclassname.AutoSize = true;
            this.lblclassname.Location = new System.Drawing.Point(299, 26);
            this.lblclassname.Name = "lblclassname";
            this.lblclassname.Size = new System.Drawing.Size(53, 12);
            this.lblclassname.TabIndex = 3;
            this.lblclassname.Text = "班级名称";
            this.lblclassname.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(299, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "年   级";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "备注";
            // 
            // lblid
            // 
            this.lblid.AutoSize = true;
            this.lblid.Location = new System.Drawing.Point(35, 23);
            this.lblid.Name = "lblid";
            this.lblid.Size = new System.Drawing.Size(53, 12);
            this.lblid.TabIndex = 0;
            this.lblid.Text = "班级编号";
            // 
            // frmclass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 374);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmclass";
            this.Text = "班级信息";
            this.Load += new System.EventHandler(this.frmclass_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvclass)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblclassname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblid;
        private System.Windows.Forms.Button btnre;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.TextBox txtremark;
        private System.Windows.Forms.ComboBox cboclass;
        private System.Windows.Forms.DateTimePicker dtpopen;
        private System.Windows.Forms.TextBox txtclassname;
        private System.Windows.Forms.TextBox txtid;
        private System.Windows.Forms.DataGridView dgvclass;
    }
}