﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StudentInfoMS
{
    public partial class FrmScore : Form
    {
        public FrmScore()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void FrmScore_Load(object sender, EventArgs e)
        {
            loadScoreTables();
            loadStudent();
            loadSubject();
        }

        private void loadSubject()
        {
            //科目下拉列表
            string sql = " select * from dbo.tb_courses";
            DataSet da = DatabaseHelper.executeDataSet(sql);
            cbosubject.DataSource = da.Tables[0];
            cbosubject.DisplayMember = "courseName";
            cbosubject.ValueMember = "id";
        }

        private void loadStudent()
        {//学生下拉列表
            string sql = " select * from tb_students";
            DataSet da = DatabaseHelper.executeDataSet(sql);
            cbostudent.DataSource = da.Tables[0];
            cbostudent.DisplayMember = "studentName";
            cbostudent.ValueMember = "id";
        }

        private void loadScoreTables()
        {
            //加载分数表
            string sql = " select  s.id  编号,tb_students.studentName 学生,tb_courses.courseName 科目名称,s.examineTime 考试时间,s.score 分数,s.remark 备注"
                + " from tb_students,tb_courses,tb_scores as s"
                + " where tb_students.id=s.studentId and tb_courses.id=s.courseId";
            DataSet da = DatabaseHelper.executeDataSet(sql);
            dgvscore.DataSource = da.Tables[0];
        }

        private void dgvscore_CellClick(object sender, DataGridViewCellEventArgs e)
        {//显示在上面控件
            txtscoreid.Text = dgvscore.SelectedRows[0].Cells[0].Value.ToString();
            txtscore.Text = dgvscore.SelectedRows[0].Cells[4].Value.ToString();
            txtremark.Text = dgvscore.SelectedRows[0].Cells[5].Value.ToString();
            cbostudent.Text = dgvscore.SelectedRows[0].Cells[1].Value.ToString();
            cbosubject.Text = dgvscore.SelectedRows[0].Cells[2].Value.ToString();
            dtptime.Text = dgvscore.SelectedRows[0].Cells[3].Value.ToString();
        }

        private void btnre_Click(object sender, EventArgs e)
        {//重填
            txtscoreid.Text = "";
            txtscore.Text = "";
            txtremark.Text = "";
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            //删除
            string sql = " delete tb_scores where id=" + dgvscore.SelectedRows[0].Cells[0].Value.ToString();
            int num = DatabaseHelper.executeNonQuery(sql);
            if (num > 0)
            {
                MessageBox.Show("删除成功");
            }
            else
            {
                MessageBox.Show("删除失败!");
            } loadScoreTables();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            //保存
            string sql;
            if (txtscoreid.Text.Trim() != "")
            {//添加
               sql = string.Format(" insert into tb_scores values ('{0}','{1}','{2}','{3}','{4}')",
                    cbostudent.SelectedValue, cbosubject.SelectedValue, dtptime.Value.ToShortDateString(), txtscore.Text, txtremark.Text);

            }
            else
            {
                //更新
                sql = string.Format(" update tb_scores set studentId='{0}',courseId='{1}',examineTime='{2}',score='{3}',remark='{4}' where id='{5}'",
                    cbostudent.SelectedValue, cbosubject.SelectedValue, dtptime.Value.ToShortDateString(), txtscore.Text, txtremark.Text, dgvscore.SelectedRows[0].Cells[0].Value.ToString());
            }
            int num = DatabaseHelper.executeNonQuery(sql);
            if (num > 0)
            {
                MessageBox.Show("保存成功");
            }
            else
            {
                MessageBox.Show("保存失败!");
            }
            loadScoreTables();
        }

        private void btnselect_Click(object sender, EventArgs e)
        {
            //查询
            string sql = " select  s.id  编号,tb_students.studentName 学生,tb_courses.courseName 科目名称,s.examineTime 考试时间,s.score 分数,s.remark 备注"
               + " from tb_students,tb_courses,tb_scores as s"
               + " where tb_students.id=s.studentId and tb_courses.id=s.courseId";
            if (cbstudnet.Checked)
            {
                sql += " and tb_students.id="+cbostudent.SelectedValue;
            }
            if (cbclass.Checked)
            {
                sql += " and tb_courses.id="+ cbosubject.SelectedValue;
            }
            if (cbtime.Checked)
            {
                 sql += " and s.examineTime="+ dtptime.Value.ToShortTimeString();
            }
            DataSet da = DatabaseHelper.executeDataSet(sql);
            dgvscore.DataSource = da.Tables[0];
        }

    }
}
