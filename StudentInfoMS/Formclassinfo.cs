﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace StudentInfoMS
{
    public partial class frmclass : Form
    {
        public frmclass()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
         public  DataSet ds =null;
       public  SqlDataAdapter da = null;

       private void frmclass_Load(object sender, EventArgs e)
       {
           loadclass();
       }
       public void loadclass()
       {//加载班级方法
           ds = new DataSet();
           string sql = "  select id as id, className as 班级名称,openDate as 开班时间,grade as 年级,remark as 备注 from tb_classes";
           da = new SqlDataAdapter(sql, DatabaseHelper.connection);
           da.Fill(ds);
           dgvclass.DataSource = ds.Tables[0];

       }

       private void dgvclass_CellClick(object sender, DataGridViewCellEventArgs e)
       {//选中的显示在上面
           this.txtid.Text = dgvclass.SelectedRows[0].Cells[0].Value.ToString();
           this.txtclassname.Text = dgvclass.SelectedRows[0].Cells[1].Value.ToString();
           this.dtpopen.Text = dgvclass.SelectedRows[0].Cells[2].Value.ToString();
           this.cboclass.Text = dgvclass.SelectedRows[0].Cells[3].Value.ToString();
           this.txtremark.Text = dgvclass.SelectedRows[0].Cells[4].Value.ToString();
       }

       private void btnsave_Click(object sender, EventArgs e)
       {
           
           string sql="";
           if (txtid.Text.Trim()!="")
           {//更新
                sql = string.Format("update tb_classes set classname='{0}', openDate ='{1}',grade ='{2}',remark ='{3}' where id ='{4}'",
               txtclassname.Text,dtpopen.Value.ToString(),cboclass.Text,txtremark.Text, dgvclass.SelectedRows[0].Cells[0].Value.ToString());
           }
           else
           {//添加
               sql = string.Format("insert into tb_classes values ('{0}','{1}','{2}','{3}')",txtclassname.Text, dtpopen.Value.ToString(), cboclass.Text, txtremark.Text);
           }
                try
           {
               DatabaseHelper.connection.Open();
               SqlCommand com = new SqlCommand(sql, DatabaseHelper.connection);
               int num = com.ExecuteNonQuery();
               if (num > 0)
               {
                   MessageBox.Show("保存成功!");
               }
                }
           catch (Exception ex)
           {

               MessageBox.Show(ex.Message);
           }
           finally
           {
               DatabaseHelper.connection.Close();
               loadclass();
           }
         
           }
           
      

       private void btndelete_Click(object sender, EventArgs e)
       {
           string sql = string.Format(" delete tb_classes where id='{0}'", dgvclass.SelectedRows[0].Cells[0].Value.ToString());
           try
           {
               DatabaseHelper.connection.Open();
               SqlCommand com = new SqlCommand(sql, DatabaseHelper.connection);
               int num = com.ExecuteNonQuery();
               if (num > 0)
               {
                   MessageBox.Show("删除成功!");
               }
           }
           catch (Exception ex)
           {

               MessageBox.Show(ex.Message);
           }
           finally
           {
               DatabaseHelper.connection.Close();
           }
           loadclass();
       }

       private void btnre_Click(object sender, EventArgs e)
       {
           this.txtid.Text = "";
           this.txtclassname.Text = "";
           this.dtpopen.Text = "";
           this.cboclass.Text = "";
           this.txtremark.Text = "";
       }

      
    }
}
