﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace StudentInfoMS
{
    class DbHelper
    {
        private static string connstring = "data source=.;initial catalog=Ticket;User ID=sa;Pwd=123.com";
        public static SqlConnection connection = new SqlConnection(connstring);
        //增删改操作
        public static int executeNonQuery(String sql)
        {
            int num = 0;
            try
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(sql, connection);
                num = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return num;
        }

        //查询操作 返回DataSet
        public static DataSet executeDataSet(String sql)
        {
            DataSet dataSet = new DataSet();
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                adapter.Fill(dataSet);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return dataSet;



        }

    }
}
