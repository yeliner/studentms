﻿namespace StudentInfoMS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvclass = new System.Windows.Forms.DataGridView();
            this.btnresave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvclass)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvclass
            // 
            this.dgvclass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvclass.Location = new System.Drawing.Point(0, 0);
            this.dgvclass.Name = "dgvclass";
            this.dgvclass.RowTemplate.Height = 23;
            this.dgvclass.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvclass.Size = new System.Drawing.Size(554, 202);
            this.dgvclass.TabIndex = 0;
            // 
            // btnresave
            // 
            this.btnresave.Location = new System.Drawing.Point(479, 232);
            this.btnresave.Name = "btnresave";
            this.btnresave.Size = new System.Drawing.Size(75, 23);
            this.btnresave.TabIndex = 1;
            this.btnresave.Text = "更新";
            this.btnresave.UseVisualStyleBackColor = true;
            this.btnresave.Click += new System.EventHandler(this.btnresave_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 293);
            this.Controls.Add(this.btnresave);
            this.Controls.Add(this.dgvclass);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvclass)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvclass;
        private System.Windows.Forms.Button btnresave;
    }
}